<?php

namespace App\Service;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Client;
use App\Entity\Call;
use App\Entity\SMS;

class parseFilesService
{
    protected $path;

    public function getData(array $files, string $path = null)
    {
        $this->path = $path;
        $res = array_map([$this, 'getFiles'], $files);
        $response = [];
        array_walk($res, function ($file) use (&$response) {
            $key = array_keys($file);
            $response[$key[0]] = $file[$key[0]];
        });

        return $response;
    }

    protected function getFiles($file)
    {
        $destiny = 'https://gist.githubusercontent.com/miguelgf/e099e5e5bfde4f6428edb0ae94946c83/raw/fa27e59eb8f14ee131fca5c0c7332ff3b924e0b2/';
        try {
            $request = new Request('GET', $destiny . $file);
            $client = new Client();
            $response = $client->send($request, ['timeout' => 30]);
            $body = $response->getBody()->getContents();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage() . ' the trace ' . $e->getTraceAsString());
        }

        return $this->responseToObject($body, $file);
    }

    protected function responseToObject(string $body, $file)
    {
        $responselines = explode("\n", $body);
        unset($responselines[count($responselines)]);
        $validObjects = array_map([$this, 'factoryMessages'], $responselines);
        dump($validObjects);
        return [$file => $validObjects];
    }

    public function factoryMessages($line)
    {
        
        if (!empty($line)) {
            switch ($line[0]) {
                case 'C':
                    $call = new Call();
                    $call->parseLogToObject($line);
                    return $call;
                    break;
                case 'S':
                    $sms = new SMS();
                    $sms->parseLogToObject($line);
                    return $sms;
                    break;
                default:
                    break;
            }
        }
    }
}
