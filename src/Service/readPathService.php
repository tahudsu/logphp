<?php

namespace App\Service;

class readPathService {
    public function readFiles(string $urlPath): array
    {
        $files = [];
        $files = scandir($urlPath);
        if (!empty($files)) {
            unset($files[0]);
            unset($files[1]);
        }
        return $files;
    }
}