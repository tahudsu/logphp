<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\readPathService;
use App\Service\parseFilesService;

class DefaultController extends AbstractController {
    public function index(readPathService $readPathService, parseFilesService $parseFile)
    {
        $path = '/home/yesus/Documentos/Proyectos/testlogfinizens/';
        $files = $readPathService->readFiles($path);
        $data = [];
        if ($files) {

            $data = $parseFile->getData($files, $path);
        }
        $data = ['log' => $data];
        return $this->render('default/index.html.twig', $data);
    }
}