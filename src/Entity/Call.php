<?php

namespace App\Entity;

use App\Entity\SMS;
use Doctrine\ORM\Mapping as ORM;

class Call extends SMS
{
    /**
     *
     * @var integer
     * @ORM\Column(type="integer", length=6)
     */
    private $duracionLlamada;

    public function setDuracionLlamada(string $line): self
    {
        $this->duracionLlamada = $line;
        return $this;    
    }

    public function getDurancionLlamada()
    {
        return $this->duracionLlamada;
    }

    public function parseLogToObject(string $line)
    {
        parent::parseLogToObject($line);
        $this->setDuracionLlamada(substr($line, 58, 6));
    }
}