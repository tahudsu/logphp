<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
class SMS
{
    /**
     *
     * @var string
     * @ORM\Column(type="integer", length=9)
     */
    private $origen;
    private $destino;
    /**
     * true, entrante. false, saliente.
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $tipo;
    /**
     * 
     * @var string
     * @ORM\Column(type="string", max-length="24")
     */
    private $nombreContacto;

    private $fechaComunicacion;

    public function setOrigen(string $line): self
    {
        $this->origen = $line;
        return $this;
    }

    public function getOrigen(): string
    {
        return $this->origen;
    }

    public function setDestino(string $line): self
    {
        $this->destino = $line;
        return $this;
    }

    public function getDestino(): string
    {
        return $this->destino;
    }

    public function setTipo(bool $tipo): self
    {
        $this->tipo = $tipo;
        return $this;
    }

    public function getTipo(): bool
    {
        return $this->tipo;
    }

    public function setNombreContacto(string $line): self
    {
        $this->nombreContacto = trim($line);
        return $this;
    }

    public function getNombreContacto()
    {
        return $this->nombreContacto;
    }

    public function setFechaComunicacion(string $line): self
    {
        $this->fechaComunicacion = $line;
        return $this;
    }

    public function getFechaComunicacion(): \Date
    {
        return $this->fechaComunicacion;
    }

    public function parseLogToObject(string $line)
    {
        dump($line);
        $this->setOrigen(substr($line, 1, 9));
        $this->setDestino(substr($line, 10, 9));
        $this->setTipo(substr($line, 19, 1));
        $this->setNombreContacto(substr($line,20, 24));
        $this->setFechaComunicacion(substr($line, 44, 14));
    }
}